import Link from 'next/link'

import { useUser } from '../..//utils/auth/useUser'

import Cart from '../../statics/local_mall.svg'
import Logo from '../../statics/brand2.svg'
import Search from '../../statics/search.svg'

const links = [
    {href: "/mall", label: "Mall Lokal"},
    {href: "/promo", label: "Promo"},
    {href: "/events", label: "Events"},
    {href: "/langganan", label: "Paket Langganan"},
]

export default function TopNavbar() {
    const { user, logout } = useUser()

    return (
        <nav className="md:py-3 py-2 items-center bg-transparent">
            <ul className="flex justify-start xl:justify-around md:justify-between sm:justify-center md:pt-0 sm:pt-3">
                <div className="hidden md:block" >
                    <a href="/" className="py-2 px-4">
                    <Logo />
                    </a>
                    <div></div>
                </div>
                <div className="flex items-center w-screen md:mx-10">
                    <input className="bg-white border border-red-300 py-2 px-4 block w-full appearance-none leading-normal"  placeholder="Cari kebutuhan sehari-hari"></input>
                    <button className="py-2 px-4 bg-red-500 hover:bg-red-600">
                        <Search />
                    </button>
                </div>
                <ul className="flex justify-between items-center">
                    <div className="hidden md:mr-4 md:block" >
                        <Link href="/cart">
                            <button className="py-auto px-4 focus:outline-none">
                                <Cart />
                            </button>
                        </Link>
                    </div>
                    <div className="hidden md:block">
                        {
                        (user) ?
                            <button onClick={() => logout()} href="#" className="py-2 px-4 btn block">
                            Logout
                            </button>
                        :
                            <Link href="/auth">
                                <button className="py-2 px-4 btn block hover:bg-red-600">
                                Login
                                </button>
                            </Link>
                        }
                    </div>
                </ul>
            </ul>
            <div className="hidden md:ml-48 md:block">
                <ul className="flex space-x-4">
                    <li>
                        <a href="/category" className="font-bold text-red-500 hover:text-red-600 focus:text-red-700">
                            Kategori
                        </a>
                    </li>
                    {links.map(({ href, label }) => (
                        <li key={`${href}${label}`}>
                            <a href={href} className=" text-gray-500 hover:text-gray-600 focus:text-gray-700">
                                {label}
                            </a> 
                        </li>
                    ))}
                </ul>
            </div>
        </nav>
    )
}